package com.challenge.blog.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.blog.entity.Blog;
import com.challenge.blog.services.BlogServices;

@RestController
public class BlogController {
	
	@Autowired BlogServices blogServices;

	@PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Blog blog) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		blogServices.create(blog);
		map.put("status", 200);
		map.put("message", "Record is Saved Successfully!");
		return new ResponseEntity<>(map, HttpStatus.CREATED);
        
    }
	
	@PostMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Blog blog) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		try {
			Blog blogOld = blogServices.getById(id);
			blogOld.setBody(blog.getBody());
			blogOld.setAuthor(blog.getAuthor());
			blogOld.setTitle(blog.getTitle());
			blogServices.update(blogOld);
			map.put("status", 201);
			map.put("data", blogServices.getById(id));
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch (Exception ex) {
			map.clear();
			map.put("status", 0);
			map.put("message", "Data is not found");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}
    }
	
	@GetMapping("/getById/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		try {
			Blog blog = blogServices.getById(id);
			map.put("status", 200);
			map.put("data", blog);
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch (Exception ex) {
			map.clear();
			map.put("status", 0);
			map.put("message", "Data is not found");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}
    }
	
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		try {
			blogServices.delete(id);
			map.put("status", 200);
			map.put("message", "Record is deleted successfully!");
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch (Exception ex) {
			map.clear();
			map.put("status", 0);
			map.put("message", "Data is not found");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}
    }
}
