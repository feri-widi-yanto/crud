package com.challenge.blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.challenge.blog.entity.Blog;
import com.challenge.blog.repository.BlogRepository;

@Service
public class BlogServices {

	@Autowired
	BlogRepository blogRepository;

	public Blog create(Blog blog) {
		return blogRepository.save(blog);
	}
	
	public Blog getById(Long id) {
		Blog blog = blogRepository.findById(id).orElseThrow();
		return blog;
	}

	public Blog update(Blog blog) {
		return blogRepository.save(blog);
	}

	public ResponseEntity<?> delete(Long id) {
		blogRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
